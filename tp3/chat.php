<?php
try {
 $bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'root', '');
} catch (Exception $e) {
    exit('Erreur de connexion à la base de données.');
}



function envoyerMessage($author, $message, $bdd){
  $insertion = $bdd->prepare('INSERT INTO messages(auteur, message, date) VALUES(:auteur, :message, :date)');
  $insertion->execute(array(
    'auteur' => htmlspecialchars($author),
    'message' => htmlspecialchars($message),
    'date' => date('y-m-d')
  ));
  $insertion->closeCursor();
}
