<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Create Event</title>
</head>
<body>
  <header>
    <h1>Create an Event</h1>
  </header>
  <form method="post" action="../controller/cr_event.php" >
      <div>
          <label for="id">* Event's name :</label>
          <input type="text" name="name" id="name" />
      </div>
      <div>
          <label for="description">* Description :</label>
          <input type="text" name="description" id="description" />
      </div>
      <div>
          <label for="startdate">* Beginning of the event :</label>
          <input type="date" name="startdate" id="startdate" />
      </div>
      <div>
          <label for="enddate">* End of the event :</label>
          <input type="date" name="enddate" id="enddate" />
      </div>
      <div>
          <label for="nbplaces">* Number of places :</label>
          <input type="number" name="nbplaces" id="nbplaces" />
      </div>
      <div class="button">
          <button type="submit">Submit </button>
      </div>
  </form>
</body>
</html>
