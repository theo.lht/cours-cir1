<!doctype html>
<html lang="fr">
  <head>
      <meta charset="utf-8">
    <title>Calendar</title>
  </head>
<body>
  <header>
    <h1>Connexion to the calendar</h1>
  </header>
  <section>

    <form method="post" action="../controller/connexion.php" >
        <div>
            <label for="id">* Pseudo :</label>
            <input type="text" name="id" id="pseudo" />
        </div>
        <div>
            <label for="password">* Password :</label>
            <input type="password" name="password" id="password" />
        </div>
        <div class="button">
            <button type="submit">Submit </button>
        </div>
    </form>
  </section>
</body>
</html>
