<?php
try{
$bdd = new PDO('mysql:host=localhost;dbname=events;charset=utf8', 'root', '');
} catch (\Exception $e) {
  exit('Error Data Base');
}
if(isset($_POST['name'])&&isset($_POST['description'])&&isset($_POST['startdate'])&&isset($_POST['enddate'])&&isset($_POST['nb_place'])){
  $name=filter_input(INPUT_POST,'name',FILTER_SANITIZE_SPECIAL_CHARS);
  $description=filter_input(INPUT_POST,'description',FILTER_SANITIZE_SPECIAL_CHARS);
  $startdate=filter_input(INPUT_POST,'startdate',FILTER_CALLBACK,'options','filterdate');
  $enddate=filter_input(INPUT_POST,'enddate',FILTER_CALLBACK,'options','filterdate');
  $nb_place=filter_input(INPUT_POST,'nb_place',FILTER_SANITIZE_SPECIAL_CHARS);
}
if(isset($_POST['name']) && isset($_POST['description']) && isset($_POST['startdate']) && isset($_POST['enddate']) && isset($_POST['nb_place'])){
$req=$bdd->prepare('INSERT INTO events(name,description,startdate,enddate,organizer_id,nb_place) VALUES(:name,:description,:startdate,:enddate,:organizer_id,:nb_place)');
$req->execute(array('name'=>$_POST['name'], 'description'=>$_POST['description'], 'startdate'=> str_replace('T',' ',$_POST['startdate']), 'enddate'=> str_replace('T',' ',$_POST['enddate']), 'organizer_id'=>$_SESSION['info']['id'], 'nb_place'=>$_POST['nb_place']));
}
?>
